package com.kartik.fetch.domain.viewmodels

import com.kartik.fetch.domain.interactor.GetHiringData
import com.kartik.fetch.domain.interactor.SyncHiringData
import com.kartik.fetch.domain.models.HiringData
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.just
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import strikt.api.expectThat
import strikt.assertions.isEmpty
import strikt.assertions.isEqualTo
import strikt.assertions.isNotEmpty

@OptIn(ExperimentalCoroutinesApi::class)
class HiringListViewModelTest {
    private val getHiringData: GetHiringData = mockk()
    private val syncHiringData: SyncHiringData = mockk()
    private val testDispatcher: TestDispatcher = StandardTestDispatcher()

    private lateinit var viewModel: HiringListViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)

        coEvery { syncHiringData.performAction() } just Runs
        coEvery { getHiringData.performAction() } returns emptyList()
    }

    @After
    fun cleanup() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test on init usecases are called`() = runTest {
        viewModel = HiringListViewModel(
            getHiringData = getHiringData,
            syncHiringData = syncHiringData,
            ioDispatcher = testDispatcher
        )

        testDispatcher.scheduler.advanceUntilIdle()
        coVerify(exactly = 1) {
            syncHiringData.performAction()
            getHiringData.performAction()
        }

        expectThat(viewModel.items.value).isEmpty()
    }

    @Test
    fun `test proper data is emitted`() {
        val testData = HiringData(
            id = 1,
            listId = 2,
            name = "foo"
        )
        coEvery { getHiringData.performAction() } returns listOf(testData)
        viewModel = HiringListViewModel(
            getHiringData = getHiringData,
            syncHiringData = syncHiringData,
            ioDispatcher = testDispatcher
        )

        testDispatcher.scheduler.advanceUntilIdle()
        coVerify(exactly = 1) {
            syncHiringData.performAction()
            getHiringData.performAction()
        }

        expectThat(viewModel.items.value).isNotEmpty()

        val actual = viewModel.items.value
        expectThat(actual.size).isEqualTo(1)
        expectThat(actual.keys.first()).isEqualTo(2)
        expectThat(actual.values.size).isEqualTo(1)
        expectThat(actual.values.first().first()).isEqualTo(testData)
    }

    @Test
    fun `test empty with spaces and null name values are filtered`() {
        val testData = listOf(
            HiringData(
                id = 1,
                listId = 2,
                name = " "
            ),
            HiringData(
                id = 1,
                listId = 2,
                name = "         "
            ),
            HiringData(
                id = 1,
                listId = 2,
                name = null
            ),
            HiringData(
                id = 1,
                listId = 2,
                name = "foo"
            )
        )
        coEvery { getHiringData.performAction() } returns testData
        viewModel = HiringListViewModel(
            getHiringData = getHiringData,
            syncHiringData = syncHiringData,
            ioDispatcher = testDispatcher
        )

        testDispatcher.scheduler.advanceUntilIdle()
        coVerify(exactly = 1) {
            syncHiringData.performAction()
            getHiringData.performAction()
        }

        expectThat(viewModel.items.value).isNotEmpty()

        val actual = viewModel.items.value
        expectThat(actual.size).isEqualTo(1)
        expectThat(actual.keys.first()).isEqualTo(2)
        expectThat(actual.values.size).isEqualTo(1)
        expectThat(actual.values.first().first()).isEqualTo(testData.last())
    }

    @Test
    fun `data is sorted by name after listID (same list ID)`() {
        val testData = listOf(
            HiringData(
                id = 1,
                listId = 0,
                name = "123"
            ),
            HiringData(
                id = 1,
                listId = 0,
                name = "0001"
            ),
            HiringData(
                id = 1,
                listId = 0,
                name = "9"
            ),
            HiringData(
                id = 1,
                listId = 0,
                name = "0"
            )
        )

        coEvery { getHiringData.performAction() } returns testData
        viewModel = HiringListViewModel(
            getHiringData = getHiringData,
            syncHiringData = syncHiringData,
            ioDispatcher = testDispatcher
        )

        testDispatcher.scheduler.advanceUntilIdle()
        coVerify(exactly = 1) {
            syncHiringData.performAction()
            getHiringData.performAction()
        }

        val expectedList = listOf(
            HiringData(
                id = 1,
                listId = 0,
                name = "0"
            ),
            HiringData(
                id = 1,
                listId = 0,
                name = "0001"
            ),
            HiringData(
                id = 1,
                listId = 0,
                name = "123"
            ),
            HiringData(
                id = 1,
                listId = 0,
                name = "9"
            )
        )

        expectThat(viewModel.items.value).isNotEmpty()
        expectThat(viewModel.items.value.size).isEqualTo(1)

        val actual = viewModel.items.value[0]
        expectThat(actual).isEqualTo(expectedList)
    }

    @Test
    fun `data is sorted by list ID and then name`() {
        val testData = listOf(
            HiringData(
                id = 1,
                listId = 0,
                name = "123"
            ),
            HiringData(
                id = 9999,
                listId = 2,
                name = "0001"
            ),
            HiringData(
                id = 1,
                listId = 0,
                name = "9"
            ),
            HiringData(
                id = 1,
                listId = 3,
                name = "0"
            )
        )

        coEvery { getHiringData.performAction() } returns testData
        viewModel = HiringListViewModel(
            getHiringData = getHiringData,
            syncHiringData = syncHiringData,
            ioDispatcher = testDispatcher
        )

        testDispatcher.scheduler.advanceUntilIdle()
        coVerify(exactly = 1) {
            syncHiringData.performAction()
            getHiringData.performAction()
        }

        expectThat(viewModel.items.value).isNotEmpty()
        expectThat(viewModel.items.value.size).isEqualTo(3)

        expectThat(viewModel.items.value[0]).isEqualTo(
            listOf(
                HiringData(
                    id = 1,
                    listId = 0,
                    name = "123"
                ),
                HiringData(
                    id = 1,
                    listId = 0,
                    name = "9"
                )
            )
        )

        expectThat(viewModel.items.value[2]).isEqualTo(
            listOf(
                HiringData(
                    id = 9999,
                    listId = 2,
                    name = "0001"
                )
            )
        )

        expectThat(viewModel.items.value[3]).isEqualTo(
            listOf(
                HiringData(
                    id = 1,
                    listId = 3,
                    name = "0"
                )
            )
        )
    }
}
