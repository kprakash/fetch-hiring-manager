package com.kartik.fetch.domain.interactor

import com.kartik.fetch.domain.HiringDataRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import strikt.api.expectThat
import strikt.assertions.isEmpty

@OptIn(ExperimentalCoroutinesApi::class)
class GetHiringDataTest {
    private val repo: HiringDataRepository = mockk()

    private val interactor = GetHiringData(repo)

    @Test
    fun `interactor calls repo`() = runTest {
        coEvery { repo.getHiringData() } returns emptyList()

        expectThat(interactor.performAction()).isEmpty()
        coVerify(exactly = 1) { repo.getHiringData() }
    }
}
