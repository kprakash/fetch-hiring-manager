package com.kartik.fetch.domain.interactor

import com.kartik.fetch.domain.HiringDataRepository
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.just
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class SyncHiringDataTest {
    private val repo: HiringDataRepository = mockk()

    private val interactor = SyncHiringData(repo)

    @Test
    fun `interactor calls repo`() = runTest {
        coEvery { repo.syncHiringData() } just Runs

        interactor.performAction()

        coVerify(exactly = 1) { repo.syncHiringData() }
    }
}
