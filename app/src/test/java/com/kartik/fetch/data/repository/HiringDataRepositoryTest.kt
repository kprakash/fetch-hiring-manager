package com.kartik.fetch.data.repository

import com.kartik.fetch.data.db.FetchHiringDatabaseDao
import com.kartik.fetch.data.provider.HiringDataApi
import com.kartik.fetch.domain.HiringDataRepository
import com.kartik.fetch.domain.models.HiringData
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import strikt.api.expectThat
import strikt.assertions.isEmpty
import strikt.assertions.isEqualTo
import java.util.Optional

@OptIn(ExperimentalCoroutinesApi::class)
class HiringDataRepositoryTest {
    private val api: HiringDataApi = mockk()
    private val dao: FetchHiringDatabaseDao = mockk()

    private val repo: HiringDataRepository = HiringDataRepositoryImpl(
        api = api,
        dao = dao
    )

    @Before
    fun setup() {
        coEvery { api.getData() } returns Optional.empty<List<HiringData>>()
        every { dao.getItems() } returns emptyList()
        every { dao.saveItem(any()) } just Runs
    }

    @Test
    fun `get hiring data calls the dao`() = runTest {
        val actual = repo.getHiringData()
        expectThat(actual).isEmpty()

        coVerify(exactly = 1) {
            dao.getItems()
        }
    }

    @Test
    fun `get hiring data returns expected items`() = runTest {
        val firstItem = HiringData(id = 1, listId = 2, name = "kartik")
        val secondItem = HiringData(id = 2, listId = 3, name = "kartik123")

        every { dao.getItems() } returns listOf(firstItem, secondItem)

        val actual = repo.getHiringData()

        expectThat(actual.size).isEqualTo(2)
        expectThat(actual.first()).isEqualTo(firstItem)
        expectThat(actual[1]).isEqualTo(secondItem)
    }

    @Test
    fun `sync hiring data calls the api`() = runTest {
        repo.syncHiringData()

        coVerify(exactly = 1) {
            api.getData()
        }
        coVerify(exactly = 0) {
            dao.saveItem(any())
        }
    }

    @Test
    fun `sync calls save the data into database`() = runTest {
        val firstItem = HiringData(id = 1, listId = 2, name = "kartik")
        val secondItem = HiringData(id = 2, listId = 3, name = "kartik123")

        coEvery { api.getData() } returns Optional.of(listOf(firstItem, secondItem))

        repo.syncHiringData()

        coVerify(exactly = 1) {
            api.getData()
            dao.saveItem(firstItem)
            dao.saveItem(secondItem)
        }
    }
}
