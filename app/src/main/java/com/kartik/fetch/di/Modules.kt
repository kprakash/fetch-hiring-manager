package com.kartik.fetch.di

import androidx.room.Room
import com.kartik.fetch.data.db.FetchHiringDatabase
import com.kartik.fetch.data.provider.HiringDataApi
import com.kartik.fetch.data.repository.HiringDataRepositoryImpl
import com.kartik.fetch.domain.HiringDataRepository
import com.kartik.fetch.domain.interactor.GetHiringData
import com.kartik.fetch.domain.interactor.SyncHiringData
import com.kartik.fetch.domain.viewmodels.HiringListViewModel
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val networkModule = module {
    single {
        HttpClient(Android) {
            install(JsonFeature) {
                serializer = KotlinxSerializer(
                    kotlinx.serialization.json.Json {
                        prettyPrint = true
                    }
                )
            }
        }
    }

    factory {
        HiringDataApi(
            client = get()
        )
    }
}

val storageModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            FetchHiringDatabase::class.java,
            "hiring_database_db"
        ).build()
    }

    single {
        get<FetchHiringDatabase>().dao()
    }

    single<HiringDataRepository> {
        HiringDataRepositoryImpl(
            api = get(),
            dao = get()
        )
    }
}

val viewModule = module {
    viewModel {
        HiringListViewModel(
            getHiringData = get(),
            syncHiringData = get()
        )
    }
}

val useCaseModule = module {
    factory {
        GetHiringData(
            repo = get()
        )
    }

    factory {
        SyncHiringData(
            repository = get()
        )
    }
}
