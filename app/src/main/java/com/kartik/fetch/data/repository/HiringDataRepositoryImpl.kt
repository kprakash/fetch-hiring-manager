package com.kartik.fetch.data.repository

import com.kartik.fetch.data.db.FetchHiringDatabaseDao
import com.kartik.fetch.data.provider.HiringDataApi
import com.kartik.fetch.domain.HiringDataRepository
import com.kartik.fetch.domain.models.HiringData

class HiringDataRepositoryImpl(
    private val api: HiringDataApi,
    private val dao: FetchHiringDatabaseDao
) : HiringDataRepository {
    override suspend fun getHiringData(): List<HiringData> {
        return dao.getItems()
    }

    override suspend fun syncHiringData() {
        val result = api.getData()

        if (result.isPresent) {
            val items = result.get()
            items.forEach {
                dao.saveItem(it)
            }
        }
    }
}
