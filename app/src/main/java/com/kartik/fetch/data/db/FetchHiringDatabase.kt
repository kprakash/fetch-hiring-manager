package com.kartik.fetch.data.db

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.RoomDatabase
import com.kartik.fetch.domain.models.HiringData

@Database(
    entities = [
        HiringData::class
    ],
    version = 1
)
abstract class FetchHiringDatabase : RoomDatabase() {
    abstract fun dao(): FetchHiringDatabaseDao
}

@Dao
interface FetchHiringDatabaseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveItem(vararg items: HiringData)

    @Query("SELECT * FROM hiring_data")
    fun getItems(): List<HiringData>
}
