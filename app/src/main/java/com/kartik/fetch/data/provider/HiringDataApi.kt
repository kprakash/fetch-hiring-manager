package com.kartik.fetch.data.provider

import android.util.Log
import com.kartik.fetch.LOG_TAG
import com.kartik.fetch.domain.models.HiringData
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.http.HttpMethod
import java.util.Optional

class HiringDataApi(
    private val client: HttpClient
) {
    suspend fun getData(): Optional<List<HiringData>> {
        val request = request {
            method = HttpMethod.Get
            url(HIRING_DATA_PATH)
        }

        return try {
            val items = client.get<List<HiringData>>(request)
            Optional.of(items)
        } catch (e: Exception) {
            Log.e(LOG_TAG, "Some error occurred when fetching data", e)
            return Optional.empty()
        }
    }

    private companion object {
        const val BASE_URL = "https://fetch-hiring.s3.amazonaws.com"
        const val HIRING_DATA_PATH = "$BASE_URL/hiring.json"
    }
}
