package com.kartik.fetch.ui.hiring

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.kartik.fetch.R
import com.kartik.fetch.domain.models.HiringData

@Composable
fun HeaderView(listID: Int) {
    Box(
        modifier = Modifier
            .background(Color.Blue)
            .padding(8.dp)
    ) {
        Text(
            text = stringResource(id = R.string.list_id_title, listID),
            fontSize = 17.sp,
            color = Color.White,
            modifier = Modifier
                .fillMaxWidth()
        )
    }
}

@Composable
fun ItemView(listItems: List<HiringData>) {
    Column {
        listItems.forEach {
            Text(
                text = stringResource(id = R.string.item_id_and_name, it.id, it.name!!),
                fontSize = 16.sp,
                color = Color.DarkGray,
                modifier = Modifier
            )
        }
    }
}

// TODO make it an expandable list
@Composable
fun GroupListContainerView(items: List<HiringData>) {
    val id = items.first().listId
    Column {
        HeaderView(listID = id.toInt())
        ItemView(listItems = items)
    }
}

@Preview(showBackground = true)
@Composable
fun ListPreview() {
    GroupListContainerView(
        listOf(
            HiringData(
                id = 4,
                name = "kartik",
                listId = 2
            ),
            HiringData(
                id = 1,
                name = "kartik",
                listId = 2
            ),
            HiringData(
                id = 6,
                name = "kartik",
                listId = 2
            ),
            HiringData(
                id = 7,
                name = "kartik",
                listId = 2
            )
        )
    )
}
