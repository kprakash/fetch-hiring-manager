package com.kartik.fetch.ui.hiring

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import com.kartik.fetch.domain.viewmodels.HiringListViewModel
import org.koin.androidx.compose.get

@Composable
fun HiringListPage() {
    val viewModel: HiringListViewModel = get()
    val itemsList by viewModel.items.collectAsState()

    Scaffold { padding ->
        LazyColumn(modifier = Modifier.padding(padding)) {
            itemsIndexed(itemsList.toList()) { _, item ->
                GroupListContainerView(
                    items = item.second
                )
            }
        }
    }
}
