package com.kartik.fetch.domain

import com.kartik.fetch.domain.models.HiringData

interface HiringDataRepository {
    suspend fun getHiringData(): List<HiringData>
    suspend fun syncHiringData()
}
