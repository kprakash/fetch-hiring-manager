package com.kartik.fetch.domain.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kartik.fetch.domain.interactor.GetHiringData
import com.kartik.fetch.domain.interactor.SyncHiringData
import com.kartik.fetch.domain.models.HiringData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HiringListViewModel(
    private val getHiringData: GetHiringData,
    private val syncHiringData: SyncHiringData,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.Default
) : ViewModel() {
    private val itemsList = MutableStateFlow(mapOf<Long, List<HiringData>>())
    val items: StateFlow<Map<Long, List<HiringData>>> get() = itemsList

    init {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                syncHiringData.performAction()
                getHiringData.performAction()
                    .filter { it.isValid() }
                    .sortedWith(compareBy({ it.listId }, { it.name }))
                    .groupBy { it.listId }
                    .let {
                        itemsList.emit(it)
                    }
            }
        }
    }

    private fun HiringData.isValid(): Boolean {
        return name?.trim()?.isNotEmpty() ?: false
    }
}
