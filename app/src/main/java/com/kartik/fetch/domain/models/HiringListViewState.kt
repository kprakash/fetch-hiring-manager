package com.kartik.fetch.domain.models

import kotlinx.coroutines.flow.StateFlow

sealed class HiringListViewState {
    object EmptyList : HiringListViewState()

    data class DataAvailable(
        val items: StateFlow<List<HiringData>>
    ) : HiringListViewState()
}
