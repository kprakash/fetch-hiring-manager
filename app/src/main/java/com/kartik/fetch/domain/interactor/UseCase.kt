package com.kartik.fetch.domain.interactor

interface UseCase<out R> {
    suspend fun performAction(): R
}
