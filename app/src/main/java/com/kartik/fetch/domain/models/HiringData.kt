package com.kartik.fetch.domain.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// TODO is `id` really primary key?

@Serializable
@Entity(tableName = "hiring_data")
data class HiringData(
    @SerialName("id")
    @ColumnInfo(name = "id")
    @PrimaryKey
    val id: Long,

    @SerialName("listId")
    @ColumnInfo(name = "list_id")
    val listId: Long,

    @SerialName("name")
    @ColumnInfo(name = "name")
    val name: String?
)
