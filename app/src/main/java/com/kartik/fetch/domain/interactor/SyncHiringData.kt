package com.kartik.fetch.domain.interactor

import com.kartik.fetch.domain.HiringDataRepository

class SyncHiringData(private val repository: HiringDataRepository) : UseCase<Unit> {
    override suspend fun performAction() {
        repository.syncHiringData()
    }
}
