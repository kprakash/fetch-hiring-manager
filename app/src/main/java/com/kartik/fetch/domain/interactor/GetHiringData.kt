package com.kartik.fetch.domain.interactor

import com.kartik.fetch.domain.HiringDataRepository
import com.kartik.fetch.domain.models.HiringData

class GetHiringData(private val repo: HiringDataRepository) : UseCase<List<HiringData>> {
    override suspend fun performAction(): List<HiringData> {
        return repo.getHiringData()
    }
}
