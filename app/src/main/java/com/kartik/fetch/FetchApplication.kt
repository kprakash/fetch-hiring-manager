package com.kartik.fetch

import android.app.Application
import com.kartik.fetch.di.networkModule
import com.kartik.fetch.di.storageModule
import com.kartik.fetch.di.useCaseModule
import com.kartik.fetch.di.viewModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class FetchApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(applicationContext)
            loadKoinModules(
                listOf(
                    networkModule,
                    storageModule,
                    viewModule,
                    useCaseModule
                )
            )
        }
    }
}
