package com.kartik.fetch

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.kartik.fetch.ui.hiring.HiringListPage
import com.kartik.fetch.ui.theme.FetchHiringManagerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FetchHiringManagerTheme {
                HiringListPage()
            }
        }
    }
}
