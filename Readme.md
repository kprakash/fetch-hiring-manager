# Fetch Hiring Manager Application

## How to run application

1. Attach your android phone or start the emlator
2. Run the gradle command `./gradlew app:installDebug

OR

1. Open the project in android studio
2. Connect your phone or launch emulator
3. Run app using the play icon in android studio

## Demo

![Demo GIF](fetch-hiring-app.gif)